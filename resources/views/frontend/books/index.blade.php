@extends('layouts.main')
@section('styles')
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
@endsection

@section('content')
    <section class="Quicksand">
        @include('frontend.partials.slider')
        <div class="container">
            <div class="row">
                <div class="col-md-8" style="margin-bottom: 10px; padding: 0px;">
                    <div class="col-md-8" style="padding: 0px;">
                        <h1 style="margin: 0px;">Boeken</h1>
                    </div>
                    <div class="col-md-4" style="padding: 0px;">
                        <a href="/boeken/toevoegen" class="btn btn-default pull-right">Nieuw boek</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="row itemBook">
                        <div class="row">
                            <div class="col-sm-8">
                                <h2>Het lelijke eendje</h2>
                            </div>
                            <div class="col-sm-4 text-right" style="font-weight: bold;">
                                <span>Thriller</span><br>
                                <span>Horror</span><br>
                                <span>Avontuur</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 text-right" style="font-weight: bold;">
                                <hr>
                                <span>Auteur</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".owl-carousel").owlCarousel({
                autoWidth: true,
                loop: true
            });
        });
    </script>
@endsection