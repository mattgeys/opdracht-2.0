@extends('layouts.main')
@section('styles')
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/slick/slick-theme.css"/>
@endsection

@section('content')
    <div id="mySidenav" class="sidenav">
        <a class="LemonMilk">Slider 1</a>
        <a class="Quicksand">Slider 2</a>
        <a class="BebasNeue">Navigatie bars</a>
        <a class="Elevation">Contact</a>
    </div>
    <div id="main">
        <section class="LemonMilk" id="slider1">
            <div class="owl-carousel owl-theme" style="margin-top: 50px;">
                <div class="item"><img src="{{Storage::url('fox.jpg')}}" alt="space"></div>
                <div class="item"><img src="{{Storage::url('lion.jpg')}}" alt="space"></div>
                <div class="item"><img src="{{Storage::url('panda.jpg')}}" alt="space"></div>
                <div class="item"><img src="{{Storage::url('red_panda.jpg')}}" alt="space"></div>
                <div class="item"><img src="{{Storage::url('squirrel.jpg')}}" alt="space"></div>
                <div class="item"><img src="{{Storage::url('turtle.jpg')}}" alt="space"></div>
                <div class="item"><img src="{{Storage::url('zebras.jpg')}}" alt="space"></div>
            </div>
            <div class="container">
                <h1>Lemon/Milk font</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi at, dolore eligendi
                    laboriosam
                    laborum minus modi veritatis. Atque consequuntur culpa ea error ipsam labore quos suscipit
                    voluptatibus.
                    Dignissimos, quod.</p>
            </div>
        </section>
        <section class="Quicksand" id="slider2">
            <div class="container">
                <h1>Quicksand font</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi at, dolore eligendi
                    laboriosam
                    laborum minus modi veritatis. Atque consequuntur culpa ea error ipsam labore quos suscipit
                    voluptatibus.
                    Dignissimos, quod.</p>
                <br>
                <div class="slider-for">
                    <div class="item"><img src="{{Storage::url('fox.jpg')}}" alt="space"></div>
                    <div class="item"><img src="{{Storage::url('lion.jpg')}}" alt="space"></div>
                    <div class="item"><img src="{{Storage::url('panda.jpg')}}" alt="space"></div>
                    <div class="item"><img src="{{Storage::url('red_panda.jpg')}}" alt="space"></div>
                    <div class="item"><img src="{{Storage::url('squirrel.jpg')}}" alt="space"></div>
                    <div class="item"><img src="{{Storage::url('turtle.jpg')}}" alt="space"></div>
                    <div class="item"><img src="{{Storage::url('zebras.jpg')}}" alt="space"></div>
                </div>
                <br>
                <div class="slider-nav">
                    <div class="item"><img src="{{Storage::url('fox.jpg')}}" alt="space"></div>
                    <div class="item"><img src="{{Storage::url('lion.jpg')}}" alt="space"></div>
                    <div class="item"><img src="{{Storage::url('panda.jpg')}}" alt="space"></div>
                    <div class="item"><img src="{{Storage::url('red_panda.jpg')}}" alt="space"></div>
                    <div class="item"><img src="{{Storage::url('squirrel.jpg')}}" alt="space"></div>
                    <div class="item"><img src="{{Storage::url('turtle.jpg')}}" alt="space"></div>
                    <div class="item"><img src="{{Storage::url('zebras.jpg')}}" alt="space"></div>
                </div>
            </div>
        </section>
        <section id="navbars">
            <div class="container">
                <h1>Bebas Neue font</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi at, dolore eligendi
                    laboriosam
                    laborum minus modi veritatis. Atque consequuntur culpa ea error ipsam labore quos suscipit
                    voluptatibus.
                    Dignissimos, quod.</p>
                <div class="row">
                    <nav class="navbar-purple navbar-default">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand">Opdracht 2</a>
                            </div>
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li class="LemonMilk"><a>Slider 1</a></li>
                                    <li class="Quicksand"><a>Slider 2</a></li>
                                    <li class="BebasNeue"><a>Navigatie bars</a></li>
                                    <li class="Elevation"><a>Contact</a></li>
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </nav>
                </div>
                <br>
                <div class="row">
                    <nav class="navbar-blue navbar-default">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand">Opdracht 2</a>
                            </div>
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li class="LemonMilk"><a>Slider 1</a></li>
                                    <li class="Quicksand"><a>Slider 2</a></li>
                                    <li class="BebasNeue"><a>Navigatie bars</a></li>
                                    <li class="Elevation"><a>Contact</a></li>
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </nav>
                </div>
                <br>
                <div class="row">
                    <nav class="navbar-red navbar-default">
                        <div class="container">
                            <div class="navbar-header">
                                <button id="toggleSidebar" type="button" class="navbar-toggle" onclick="openNav()">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </section>
        <section id="contact">
            <div class="container">
                <h1>Elevation font</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi at, dolore eligendi
                    laboriosam
                    laborum minus modi veritatis. Atque consequuntur culpa ea error ipsam labore quos suscipit
                    voluptatibus.
                    Dignissimos, quod.</p>
                <h2 class="text-center" style="margin-top: 50px;">Contact formulier</h2>
                <div class="col-xs-12">
                    @include('frontend.partials.errors')
                </div>
                <form method="post" action="contact_store">
                    {{csrf_field()}}
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="name">Naam <span class="required">*</span></label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="email">Email <span class="required">*</span></label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="message">Bericht <span class="required">*</span></label>
                            <textarea class="form-control" name="message">{{ old('message') }}</textarea>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-secondary">Verzenden</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="/slick/slick.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".owl-carousel").owlCarousel({
                loop: true,
                autoplay: true,
                autoplaySpeed: 5000,
                autoplayTimeout: 5100,
                autoplayHoverPause: true,
                responsive: {
                    600: {
                        items: 3
                    },
                    480: {
                        items: 2
                    },
                    0: {
                        items: 1
                    }
                }
             });

            $(".owl-carousel .item").mouseenter(function() {
                $(".owl-carousel .item").not(this).append('<div class="overlay"> </div>')
            });

            $(".owl-carousel .item").mouseleave(function() {
                $('.overlay').remove();
            });

            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-nav'
            });

            $('.slider-nav').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                centerMode: true,
                focusOnSelect: true,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }]
            });

            // Smoothscroll
            $('a[href*="#"]')
                    .not('[href="#"]')
                    .not('[href="#0"]')
                    .click(function (event) {
                        if (
                                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                                &&
                                location.hostname == this.hostname
                        ) {
                            // Figure out element to scroll to
                            var target = $(this.hash);
                            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                            // Does a scroll target exist?
                            if (target.length) {
                                // Only prevent default if animation is actually gonna happen
                                event.preventDefault();
                                $('html, body').animate({
                                    scrollTop: target.offset().top
                                }, 1000, function () {
                                    // Callback after animation
                                    // Must change focus!
                                    var $target = $(target);
                                    $target.focus();
                                    if ($target.is(":focus")) { // Checking if the target was focused
                                        return false;
                                    } else {
                                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                        $target.focus(); // Set focus again
                                    }
                                    ;
                                });
                            }
                        }
                    });
        });
    </script>

    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
            document.getElementById("main").style.marginLeft = "250px";
            document.getElementById("toggleSidebar").setAttribute('onclick', 'closeNav()')
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
            document.getElementById("toggleSidebar").setAttribute('onclick', 'openNav()')
        }
    </script>
@endsection