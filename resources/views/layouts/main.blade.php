<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/style.css">
    @yield('styles')
    <title>LaravelOpdracht 2</title>
</head>
<body>
@include('frontend.partials.nav')
@yield('content')


<script src="/js/app.js"></script>
@yield('scripts')
</body>
</html>
