<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactFormRequest;

class ContactController extends Controller
{
    public function store(ContactFormRequest $request)
    {
        \Mail::send('emails.contact',
            array(
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'user_message' => $request->get('message')
            ), function($message)
            {
                $message->from('matthias@webclix.be');
                $message->to('matthias@webclix.be', 'Geysen@webclix')->subject('Contactformulier');
            });

        return redirect('/' . '#contact')->with('message', 'Je bericht is verzonden!');

    }
}
