<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'title',
        'author_id'
    ];

    protected $guarded = [
        'id'
    ];

    public function genres() {
        return $this->belongsToMany(Genre::class);
    }

    public function author() {

        return $this->belongsTo(Author::class);

    }
}
