<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $fillable = [
        'name'
    ];

    protected $guarded = [
        'id'
    ];

    public function books() {
        return $this->belongsToMany(Book::class);
    }
}
